#include "Gwen/Gwen.h"
#include "Gwen/Platform.h"
#include "Gwen/Controls/TreeControl.h"
#include "Gwen/Controls/RadioButtonController.h"
#include "Gwen/Controls/VerticalSlider.h"
#include "Gwen/Controls/HorizontalSlider.h"
#include "Gwen/Controls/GroupBox.h"
#include "Gwen/Controls/CheckBox.h"
#include "Gwen/Controls/MenuStrip.h"
#include "Gwen/Align.h"
#include "Gwen/Utility.h"
#include "Gwen/Controls/WindowControl.h"
#include "Gwen/Controls/TabControl.h"
#include "Gwen/Controls/ListBox.h"
#include "Gwen/Controls/HorizontalSlider.h"
#include "Gwen/Controls/TextBox.h"

#include "rendertest/gwenWindow.h"
#include "OpenGLTrueTypeFont/fontstash.h"
#include "rendertest/GwenOpenGL3CoreRenderer.h"
#include "rendertest/GLPrimitiveRenderer.h"

#include "OpenGLTrueTypeFont/opengl_fontstashcallbacks.h"

GLPrimitiveRenderer* primRenderer=0;
GwenOpenGL3CoreRenderer* pRenderer = 0;
Gwen::Skin::Simple skin;
Gwen::Controls::Canvas* pCanvas =0;
class MyProfileWindow* profWindow = 0;

Gwen::Controls::Label* pLabelForBroadPhase = 0;
Gwen::Controls::Label* pLabelForBroadPhaseCL = 0;
Gwen::Controls::Button* pButtonToggleSimulation = 0;
Gwen::Controls::Button* pButtonOneStepSimulation = 0;
Gwen::Controls::Button* pButtonShowBBox = 0;
Gwen::Controls::Button* pButtonReset = 0;

extern bool g_bSimulationPause;
extern bool g_bSimulationOneStep;
extern bool g_bShowBBox;
extern bool g_bReset;

int droidRegular, droidItalic, droidBold, droidJapanese, dejavu;

sth_stash* initFont()
{
	GLint err;

		struct sth_stash* stash = 0;
	int datasize;
	unsigned char* data;
	//float sx,sy,dx,dy,lh;
	//GLuint texture;

	stash = sth_create(512,512,OpenGL2UpdateTextureCallback,OpenGL2RenderCallback);//256,256);//,1024);//512,512);
    err = glGetError();
    assert(err==GL_NO_ERROR);
    
	if (!stash)
	{
		fprintf(stderr, "Could not create stash.\n");
		return 0;
	}

	const char* fontPaths[]={
	"./",
	"../../bin/",
	"../bin/",
	"bin/"
	};

	int numPaths=sizeof(fontPaths)/sizeof(char*);
	
	// Load the first truetype font from memory (just because we can).
    
	FILE* fp = 0;
	const char* fontPath ="./";
	char fullFontFileName[1024];

	for (int i=0;i<numPaths;i++)
	{
		
		fontPath = fontPaths[i];
		//sprintf(fullFontFileName,"%s%s",fontPath,"OpenSans.ttf");//"DroidSerif-Regular.ttf");
		sprintf(fullFontFileName,"%s%s",fontPath,"DroidSerif-Regular.ttf");//OpenSans.ttf");//"DroidSerif-Regular.ttf");
		fp = fopen(fullFontFileName, "rb");
		if (fp)
			break;
	}

    err = glGetError();
    assert(err==GL_NO_ERROR);
    
    assert(fp);
    if (fp)
    {
        fseek(fp, 0, SEEK_END);
        datasize = (int)ftell(fp);
        fseek(fp, 0, SEEK_SET);
        data = (unsigned char*)malloc(datasize);
        if (data == NULL)
        {
            assert(0);
            return 0;
        }
        else
            fread(data, 1, datasize, fp);
        fclose(fp);
        fp = 0;
    }
	if (!(droidRegular = sth_add_font_from_memory(stash, data)))
    {
        assert(0);
        return 0;
    }
    err = glGetError();
    assert(err==GL_NO_ERROR);

	// Load the remaining truetype fonts directly.
    sprintf(fullFontFileName,"%s%s",fontPath,"DroidSerif-Italic.ttf");

	if (!(droidItalic = sth_add_font(stash,fullFontFileName)))
	{
        assert(0);
        return 0;
    }
     sprintf(fullFontFileName,"%s%s",fontPath,"DroidSerif-Bold.ttf");

	if (!(droidBold = sth_add_font(stash,fullFontFileName)))
	{
        assert(0);
        return 0;
    }
    err = glGetError();
    assert(err==GL_NO_ERROR);
    
     sprintf(fullFontFileName,"%s%s",fontPath,"DroidSansJapanese.ttf");
    if (!(droidJapanese = sth_add_font(stash,fullFontFileName)))
	{
        assert(0);
        return 0;
    }
    err = glGetError();
    assert(err==GL_NO_ERROR);

	return stash;
}

class MyProfileWindow : public Gwen::Controls::WindowControl
{
	
	//		Gwen::Controls::TabControl*	m_TabControl;
	//Gwen::Controls::ListBox*	m_TextOutput;
	unsigned int				m_iFrames;
	float						m_fLastSecond;

	Gwen::Controls::TreeNode* m_node;
	Gwen::Controls::TreeControl* m_ctrl;


protected:

	void onButtonToggleSimulation( Gwen::Controls::Base* pControl )
	{
		g_bSimulationPause = !g_bSimulationPause;
	}

	void onButtonStepSimulation( Gwen::Controls::Base* pControl )
	{
		g_bSimulationPause = false;
		g_bSimulationOneStep = true;	
	}

	void onButtonToggleBBox( Gwen::Controls::Base* pControl )
	{
		g_bShowBBox = !g_bShowBBox;
	}

	void onButtonReset( Gwen::Controls::Base* pControl )
	{
		g_bReset = true;
	}

public:

		void MenuItemSelect(Gwen::Controls::Base* pControl)
	{
		if (Hidden())
		{
			SetHidden(false);
		} else
		{
			SetHidden(true);
		}
	}


	MyProfileWindow (	Gwen::Controls::Base* pParent)
		: Gwen::Controls::WindowControl( pParent )
	{
		SetTitle( L"Simulation" );

		SetSize( 300, 150 );
		this->SetPos(10,40);
		
		this->Dock( Gwen::Pos::Bottom);

		{
			m_ctrl = new Gwen::Controls::TreeControl( this );
			m_node = m_ctrl->AddNode( L"Total Parent Time" );
		
			//Gwen::Controls::TreeNode* pNode = ctrl->AddNode( L"Node Two" );
			//pNode->AddNode( L"Node Two Inside" );
			//pNode->AddNode( L"Eyes" );
			//pNode->AddNode( L"Brown" )->AddNode( L"Node Two Inside" )->AddNode( L"Eyes" )->AddNode( L"Brown" );
			//Gwen::Controls::TreeNode* node = ctrl->AddNode( L"Node Three" );

			//m_ctrl->Dock(Gwen::Pos::Bottom);
			
			m_ctrl->ExpandAll();
			m_ctrl->SetBounds( this->GetInnerBounds().x,this->GetInnerBounds().y,this->GetInnerBounds().w,this->GetInnerBounds().h);
		}

		pLabelForBroadPhase = new Gwen::Controls::Label(this);
		pLabelForBroadPhase->SetPos(30, 20);

		pLabelForBroadPhaseCL = new Gwen::Controls::Label(this);
		pLabelForBroadPhaseCL->SetPos(30, 50);

		pButtonToggleSimulation = new Gwen::Controls::Button(this);
		pButtonToggleSimulation->SetPos(30, 80);
		pButtonToggleSimulation->SetIsToggle(true);
		pButtonToggleSimulation->SetText("Simulation(s)");
		pButtonToggleSimulation->onPress.Add(this, &MyProfileWindow::onButtonToggleSimulation);

		pButtonOneStepSimulation = new Gwen::Controls::Button(this);
		pButtonOneStepSimulation->SetPos(150, 80);
		pButtonOneStepSimulation->SetIsToggle(false);
		pButtonOneStepSimulation->SetText("Step(space)");
		pButtonOneStepSimulation->onPress.Add(this, &MyProfileWindow::onButtonStepSimulation);

		pButtonShowBBox = new Gwen::Controls::Button(this);
		pButtonShowBBox->SetPos(270, 80);
		pButtonShowBBox->SetIsToggle(true);
		pButtonShowBBox->SetText("BBox");
		pButtonShowBBox->onPress.Add(this, &MyProfileWindow::onButtonToggleBBox);

		pButtonReset = new Gwen::Controls::Button(this);
		pButtonReset->SetPos(380, 80);
		pButtonReset->SetIsToggle(false);
		pButtonReset->SetText("Reset");
		pButtonReset->onPress.Add(this, &MyProfileWindow::onButtonReset);
	}


	float	dumpRecursive(CProfileIterator* profileIterator, Gwen::Controls::TreeNode* parentNode)
	{
		profileIterator->First();
		if (profileIterator->Is_Done())
			return 0.f;

		float accumulated_time=0,parent_time = profileIterator->Is_Root() ? CProfileManager::Get_Time_Since_Reset() : profileIterator->Get_Current_Parent_Total_Time();
		int i;
		int frames_since_reset = CProfileManager::Get_Frame_Count_Since_Reset();
		
		//printf("Profiling: %s (total running time: %.3f ms) ---\n",	profileIterator->Get_Current_Parent_Name(), parent_time );
		float totalTime = 0.f;

	
		int numChildren = 0;
		Gwen::UnicodeString txt;
		std::vector<Gwen::Controls::TreeNode*> nodes;

		for (i = 0; !profileIterator->Is_Done(); i++,profileIterator->Next())
		{
			numChildren++;
			float current_total_time = profileIterator->Get_Current_Total_Time();
			accumulated_time += current_total_time;
			double fraction = parent_time > SIMD_EPSILON ? (current_total_time / parent_time) * 100 : 0.f;
			
			Gwen::String name(profileIterator->Get_Current_Name());
#ifdef _WIN32
			Gwen::UnicodeString uname = Gwen::Utility::StringToUnicode(name);

			txt = Gwen::Utility::Format(L"%s (%.2f %%) :: %.3f ms / frame (%d calls)",uname.c_str(), fraction,(current_total_time / (double)frames_since_reset),profileIterator->Get_Current_Total_Calls());
			
#else
			txt = Gwen::Utility::Format(L"%s (%.2f %%) :: %.3f ms / frame (%d calls)",name.c_str(), fraction,(current_total_time / (double)frames_since_reset),profileIterator->Get_Current_Total_Calls());
			
#endif

			Gwen::Controls::TreeNode* childNode = (Gwen::Controls::TreeNode*)profileIterator->Get_Current_UserPointer();
			if (!childNode)
			{
					childNode = parentNode->AddNode(L"");
					profileIterator->Set_Current_UserPointer(childNode);
			}
			childNode->SetText(txt);
			nodes.push_back(childNode);
		
			totalTime += current_total_time;
			//recurse into children
		}
	
		for (i=0;i<numChildren;i++)
		{
			profileIterator->Enter_Child(i);
			Gwen::Controls::TreeNode* curNode = nodes[i];

			dumpRecursive(profileIterator, curNode);
			
			profileIterator->Enter_Parent();
		}
		return accumulated_time;

	}

	void	UpdateText(CProfileIterator*  profileIterator, bool idle)
	{
	
		static bool update=true;

			m_ctrl->SetBounds(0,0,this->GetInnerBounds().w,this->GetInnerBounds().h);

//		if (!update)
//			return;
		update=false;

	
		static int test = 1;
		test++;

		static double time_since_reset = 0.f;
		if (!idle)
		{
			time_since_reset = CProfileManager::Get_Time_Since_Reset();
		}

		//Gwen::UnicodeString txt = Gwen::Utility::Format( L"FEM Settings  %i fps", test );
		{
		//recompute profiling data, and store profile strings


		double totalTime = 0;

		int frames_since_reset = CProfileManager::Get_Frame_Count_Since_Reset();

		profileIterator->First();

		double parent_time = profileIterator->Is_Root() ? time_since_reset : profileIterator->Get_Current_Parent_Total_Time();

	
		Gwen::Controls::TreeNode* curParent = m_node;

		double accumulated_time = dumpRecursive(profileIterator,m_node);

		const char* name = profileIterator->Get_Current_Parent_Name();
#ifdef _WIN32
		Gwen::UnicodeString uname = Gwen::Utility::StringToUnicode(name);
		Gwen::UnicodeString txt = Gwen::Utility::Format( L"Profiling: %s total time: %.3f ms, unaccounted %.3f %% :: %.3f ms", uname.c_str(), parent_time ,
			parent_time > SIMD_EPSILON ? ((parent_time - accumulated_time) / parent_time) * 100 : 0.f, parent_time - accumulated_time);
#else
		Gwen::UnicodeString txt = Gwen::Utility::Format( L"Profiling: %s total time: %.3f ms, unaccounted %.3f %% :: %.3f ms", name, parent_time ,
			parent_time > SIMD_EPSILON ? ((parent_time - accumulated_time) / parent_time) * 100 : 0.f, parent_time - accumulated_time);
#endif
		//sprintf(blockTime,"--- Profiling: %s (total running time: %.3f ms) ---",	profileIterator->Get_Current_Parent_Name(), parent_time );
		//displayProfileString(xOffset,yStart,blockTime);
		m_node->SetText(txt);


			//printf("%s (%.3f %%) :: %.3f ms\n", "Unaccounted:",);
	

		}
		
		static bool once1 = true;
		if (once1)
		{
			once1 = false;
			m_ctrl->ExpandAll();
		}

	}
	void PrintText( const Gwen::UnicodeString& str )
	{

	}

	void Render( Gwen::Skin::Base* skin )
	{
		m_iFrames++;

		if ( m_fLastSecond < Gwen::Platform::GetTimeInSeconds() )
		{
			SetTitle( Gwen::Utility::Format( L"Profiler  %i fps", m_iFrames ) );

			m_fLastSecond = Gwen::Platform::GetTimeInSeconds() + 1.0f;
			m_iFrames = 0;
		}

		Gwen::Controls::WindowControl::Render( skin );

	}


};

struct MyTestMenuBar : public Gwen::Controls::MenuStrip
{
	MyProfileWindow* m_profileWindow;


	MyTestMenuBar(Gwen::Controls::Base* pParent, MyProfileWindow* profileWindow)
		:Gwen::Controls::MenuStrip(pParent),
		m_profileWindow(profileWindow)
	{
//		Gwen::Controls::MenuStrip* menu = new Gwen::Controls::MenuStrip( pParent );
		{
			Gwen::Controls::MenuItem* pRoot = AddItem( L"File" );
		
			pRoot = AddItem( L"View" );
//			Gwen::Event::Handler* handler =	GWEN_MCALL(&MyTestMenuBar::MenuItemSelect );
			pRoot->GetMenu()->AddItem( L"Profiler",m_profileWindow,(Gwen::Event::Handler::Function)&MyProfileWindow::MenuItemSelect);

/*			pRoot->GetMenu()->AddItem( L"New", L"test16.png", GWEN_MCALL( ThisClass::MenuItemSelect ) );
			pRoot->GetMenu()->AddItem( L"Load", L"test16.png", GWEN_MCALL( ThisClass::MenuItemSelect ) );
			pRoot->GetMenu()->AddItem( L"Save", GWEN_MCALL( ThisClass::MenuItemSelect ) );
			pRoot->GetMenu()->AddItem( L"Save As..", GWEN_MCALL( ThisClass::MenuItemSelect ) );
			pRoot->GetMenu()->AddItem( L"Quit", GWEN_MCALL( ThisClass::MenuItemSelect ) );
			*/
		}
	}

};


void	setupGUI(int width, int height)
{
	sth_stash* font = initFont();
	float retinaScale = 1;

	primRenderer = new GLPrimitiveRenderer(width,height);
	pRenderer = new GwenOpenGL3CoreRenderer(primRenderer,font,width,height, retinaScale);
//	pRenderer = new Gwen::Renderer::OpenGL_DebugFont();
	skin.SetRender( pRenderer );

	pCanvas = new Gwen::Controls::Canvas( &skin );
	pCanvas->SetSize( width,height);
	pCanvas->SetDrawBackground( false);
	pCanvas->SetBackgroundColor( Gwen::Color( 150, 170, 170, 255 ) );
//	pCanvas->SetScale(.5);
	//MyWindow* window = new MyWindow(pCanvas);
	profWindow = new MyProfileWindow(pCanvas);
	
	MyTestMenuBar* menubar = new MyTestMenuBar(pCanvas, profWindow);

	/*pLabelForBroadPhase = new Gwen::Controls::Label(pCanvas);
	pLabelForBroadPhase->SetPos(30, 50);*/
	
	//Gwen::Controls::HorizontalSlider* pSlider = new Gwen::Controls::HorizontalSlider( pCanvas );
	//pSlider->SetPos( 10, 10 );
	//pSlider->SetSize( 150, 20 );
	//pSlider->SetRange( 0, 100 );
	//pSlider->SetValue( 25 );

	//Gwen::Controls::TextBox* label = new Gwen::Controls::TextBox(pCanvas);
	//label->SetText( "AAAAAAAAAAA BVBBBBBBBBBBBBBBB" );
	//label->SetPos( 10, 10 );
	////label->onTextChanged.Add( this, &ThisClass::OnEdit );
	////label->onReturnPressed.Add( this, &ThisClass::OnSubmit );
	
}

void resizeGUI(int width, int height)
{
	pCanvas->SetSize(width,height);
	pRenderer->resize(width,height);
	primRenderer->setScreenSize(width,height);
}