#include "btCpuDynamicsWorld.h"

#include "btBulletDynamicsCommon.h"
#include "BulletSoftBody/btDefaultSoftBodySolver.h"

btCpuDynamicsWorld::btCpuDynamicsWorld()
	:btSoftRigidDynamicsWorld(
			new btCollisionDispatcher(new btDefaultCollisionConfiguration()),
			new btDbvtBroadphase(),new btSequentialImpulseConstraintSolver(),
			new btDefaultCollisionConfiguration(),//todo: remove this!
			new btDefaultSoftBodySolver()
			)
{
}
	
btCpuDynamicsWorld::~btCpuDynamicsWorld()
{
	
}
