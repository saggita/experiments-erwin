
#include "GpuDemo.h"

#ifdef __APPLE__
#include "MacOpenGLWindow.h"
#else
#include "../rendering/rendertest/Win32OpenGLWindow.h"
#include "../rendering/rendertest/GLPrimitiveRenderer.h"
#endif
#include "../rendering/rendertest/GLInstancingRenderer.h"
#include "../../DemosCommon/OpenGL3CoreRenderer.h"
#include "LinearMath/btQuickProf.h"
#include "btGpuDynamicsWorld.h"
#include "HGLCamera.h"
#include "../../DemosCommon/GL_ShapeDrawer.h"
#include "BulletSoftBody/btSoftBodyHelpers.h"
#include "btGpuDynamicsWorld.h"
#include "../../../opencl/gpu_rigidbody_pipeline2/CLPhysicsDemo.h"
#include "../../../opencl/gpu_rigidbody_pipeline2/btGpuSapBroadphase.h"
#include "../../../opencl/gpu_rigidbody_pipeline/btGpuNarrowphaseAndSolver.h"

//--------------
// For Gwen GUI
//--------------
#include "SetupGUI.h"

extern btGpuNarrowphaseAndSolver* narrowphaseAndSolver;

// mouse
int g_MouseButton = 0;
int g_MouseState = 0;
bool g_bMouseWorkingOnGUI = false;

struct InternalData;
class btGpuSapBroadphase;

int g_OpenGLWidth=1024;
int g_OpenGLHeight = 768;

btgWindowInterface* window=0;

GpuDemo* demo;

HGLCamera   hCamera(threeButtons, false, 85.0f, 0.0f, -5.0f, 30.0f, 30.0f);
GLuint btmPlate;
GLuint g_glLtAxis;
GL_ShapeDrawer*	g_shapeDrawer;

// simulation start/pause/step
bool g_bSimulationPause = true;
bool g_bSimulationOneStep = false;
bool g_bShowBBox = false;
bool g_bReset = false;


// button : 0 - left, 1 - middle, 2 - right
// state : 1 - pressed, 0 - released
void MyMouseButtonCallback(int button, int state, float x, float y)
{
	g_MouseButton = button;
	g_MouseState = state;

	//btDefaultMouseButtonCallback(button,state,x,y);

	if (pCanvas)
	{
		bool handled = pCanvas->InputMouseMoved(x,y,x, y);

		if (button>=0)
		{
			bool bDown = (state == 1) ? true : false;
			handled = pCanvas->InputMouseButton(button,bDown);
			if (handled)
			{
				g_bMouseWorkingOnGUI = true;

				if (!state)
					return;
			}
		}
	}

	if ( state == 1 )
	{
		hCamera.mousePress(x, y);
	}
	else if ( state == 0 ) 
	{
		hCamera.mouseRelease(x, y);
	}		
}

void MyResizeCallback(float width, float height)
{
	g_OpenGLWidth = width;
	g_OpenGLHeight = height;
	pCanvas->SetSize(width,height);
	resizeGUI(width,height);
}

void MyMouseMoveCallback( float x, float y)
{
	btDefaultMouseMoveCallback(x,y);

	static int m_lastmousepos[2] = {0,0};
	static bool isInitialized = false;

	if (pCanvas)
	{
		if (!isInitialized)
		{
			isInitialized = true;
			m_lastmousepos[0] = x+1;
			m_lastmousepos[1] = y+1;
		}
		bool handled = pCanvas->InputMouseMoved(x,y,m_lastmousepos[0],m_lastmousepos[1]);

		if ( handled )
		{
			g_bMouseWorkingOnGUI = true;
			return;
		}
	}
	
	// if mouse is pressed
	if ( g_MouseState == 1 )
	{
		if( g_MouseButton == 0 )
		{
			hCamera.mouseMove(x, y, Left_button);
		}
		else if( g_MouseButton == 2 )
		{
			hCamera.mouseMove(x, y, Right_button);
		}		
		else if( g_MouseButton == 1 )
		{
			hCamera.mouseMove(x, y, Middle_button);
		}	
	}
}

void MyKeyboardCallback(int key, int state)
{
	if (key==BTG_ESCAPE && window)
	{
		window->setRequestExit();
	}
	else if ( key == 32 && state == 1 ) // space
	{
		g_bSimulationPause = false;
		g_bSimulationOneStep = true;	
	}
	else if ( key == 83 && state == 1 ) // 's'
	{
		g_bSimulationPause = !g_bSimulationPause;
	}

	//btDefaultKeyboardCallback(key,state);
}


GLuint GenerateAxis(float fAxisLength)
{
	GLuint list = glGenLists(1);

	GLfloat axisL = fAxisLength;
	GLfloat axis_matA[] = {0.8f, 0, 0, 0.6f };
	GLfloat axis_matB[] = {0.0, 0.0, 0.8f, 0.6f };

	glNewList(list, GL_COMPILE);	
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, axis_matA);
	glEnable(GL_BLEND);
	glDisable(GL_LIGHTING);
	glLineWidth(4);
	//glLineStipple(1, 0x0C0F);
	//glEnable(GL_LINE_STIPPLE);

	//X Axis
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(axisL, 0.0, 0.0);
	glEnd();
	glTranslatef(axisL, 0.0f, 0.0f);
	glPopMatrix();

	//Y Axis
	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_LINES);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, axisL, 0.0);	
	glEnd();

	//Z Axis
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_LINES);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, 0.0, axisL);	
	glEnd();

	glDisable(GL_BLEND);	

	glColor3f(0.0, 0.0, 0.0);

	glEnable(GL_LIGHTING);

	glLineWidth(1);
	glEndList();

	return list;
}

void InitGL()
{
	// Setting Camera Moving Sensitivity..
	hCamera.SetMouseSensitivity(0.1f, 0.02f, 0.1f);

	// Generating bottom plate...
	GLfloat gray[4] = {0.5f, 0.5f, 0.5f, 1.0f};
	GLfloat black[4] = {0.0f, 0.0f, 0.0f, 1.0f};
	btmPlate = hCamera.makeBottomPlate(gray, black, 80.0f, 80.0f, 10.0f, 0.0f);	
		
	// Generate an axis
	g_glLtAxis = GenerateAxis(5.0f);

	glViewport(0, 0, g_OpenGLWidth, g_OpenGLHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (GLfloat)g_OpenGLWidth / (GLfloat)g_OpenGLHeight, 1.f, 300.0f);
	glMatrixMode(GL_MODELVIEW);
}

void UpdateUI()
{
	if (pCanvas)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);

		pLabelForBroadPhase->SetText(Gwen::Utility::Format(L"Broadphase # of overlaps  %i", 0));
		pLabelForBroadPhase->SizeToContents();

		pLabelForBroadPhaseCL->SetText(Gwen::Utility::Format(L"Broadphase CL # of overlaps  %i", 0));
		pLabelForBroadPhaseCL->SizeToContents();

		pCanvas->RenderCanvas();
	}

	glFlush();
}

void Render()
{
	glLoadIdentity();		

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	//---------------
	//Draw background
	//---------------
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	
	glBegin(GL_QUADS);
		glColor3f(0.9F, 0.9F, 0.9F); glVertex2f( 1.0F,  1.0F);
		glColor3f(0.9F, 0.9F, 0.9F); glVertex2f(-1.0F,  1.0F);
		glColor3f(0.5F, 0.5F, 0.5F); glVertex2f(-1.0F, -1.0F);
		glColor3f(0.5F, 0.5F, 0.5F); glVertex2f( 1.0F, -1.0F);
	glEnd();
	
	glEnable(GL_DEPTH_TEST);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	//----------------
	// Applying camera
	//----------------
	hCamera.ApplyCamera();

	// Draw bottom plane
	glLineWidth(1.0f);
	glCallList(btmPlate);

	// Draw axes
	glPushMatrix();
	glCallList(g_glLtAxis);
	glPopMatrix();

	//------------
	//Set Lighting
	//------------
	GLfloat light_ambient[] = { btScalar(0.1), btScalar(0.1), btScalar(0.1), btScalar(1.0) };
	GLfloat light_diffuse[] = { btScalar(0.5), btScalar(0.5), btScalar(0.5), btScalar(1.0) };
	GLfloat light_specular[] = { btScalar(0.1), btScalar(0.1), btScalar(0.1), btScalar(1.0 )};
	GLfloat light_position0[] = { btScalar(700.0), btScalar(700.0), btScalar(700.0), btScalar(0.0 )};
	GLfloat light_position1[] = { btScalar(-700.0), btScalar(700.0), btScalar(-700.0), btScalar(0.0) };

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position0);

	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light_position1);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	// rigidbodies
	if (demo->getDynamicsWorld()->getNumCollisionObjects())
	{
		btAlignedObjectArray<btCollisionObject*> arr = demo->getDynamicsWorld()->getCollisionObjectArray();
		btCollisionObject** colObjArray = &arr[0];

		//render.renderPhysicsWorld(demo->getDynamicsWorld()->getNumCollisionObjects(),colObjArray, syncOnly);
				
		btVector3 wireColor(1.0f, 1.0f, 0.0f);
		btVector3 solidColor(0.3f, 0.1f, 0.7f);
		btVector3 aabbMin,aabbMax;
		//demo->getDynamicsWorld()->getBroadphase()->getBroadphaseAabb(aabbMin,aabbMax);

		for ( int i = 0; i < (int)arr.size(); i++ )
		{
			// solid
			btCollisionObject* colObj = arr[i];
			btScalar m[16];
			colObj->getWorldTransform().getOpenGLMatrix(m);
			g_shapeDrawer->drawOpenGL(m, colObj->getCollisionShape(), solidColor, btIDebugDraw::DBG_NoDebug,aabbMin,aabbMax);

			// wireframe
			glColor3f(1.0f, 1.0f, 0.0f);
			glPolygonOffset(1.0, 1.0);
			g_shapeDrawer->drawOpenGL(m, colObj->getCollisionShape(), wireColor, btIDebugDraw::DBG_FastWireframe,aabbMin,aabbMax);
		}

		// AABBs
		if ( g_bShowBBox )
		{
			btAlignedObjectArray<btVector3> mins;
			btAlignedObjectArray<btVector3> maxs;
			((btGpuDynamicsWorld*)demo->getDynamicsWorld())->getAabbs(mins, maxs);
			
			for ( int i = 0; i < mins.size(); i++ )
			{
				CAabb aabb;
				aabb.Set(btVector3(mins[i][0], mins[i][1], mins[i][2]), btVector3(maxs[i][0], maxs[i][1], maxs[i][2]));
				aabb.Visualize();
			}
		}
	}

	// softbodies
	/*for ( int i = 0; i < demo->getSoftBodyArray().size(); i++ )
	{
		btSoftBody*	psb=demo->getSoftBodyArray()[i];
				
		{
			btSoftBodyHelpers::Draw(psb, demo->getDynamicsWorld()->getDebugDrawer(), demo->getDynamicsWorld()->getDrawFlags());
		} 
	}*/

	for ( int i = 0; i < demo->m_ClothArray.size(); i++ )
	{
		demo->m_ClothArray[i]->Render(g_bShowBBox);
		demo->m_ClothArray[i]->RenderBatch(0);
	}

	//----------------------------------------
	// Draw normal vectors for rigidbody faces
	//----------------------------------------
	if ( 0 )
	{
		CLPhysicsDemo* gpuPhysics  = ((btGpuDynamicsWorld*)demo->getDynamicsWorld())->m_pSoftBodySolverCL->m_gpuPhysics;

		const btAlignedObjectArray<RigidBodyBase::Body>& bodyArrayCPU = *narrowphaseAndSolver->getCustomDispatchData()->m_bodyBufferCPU;
		const btAlignedObjectArray<btCollidable>& collidables = narrowphaseAndSolver->getCustomDispatchData()->m_collidablesCPU;
		const btAlignedObjectArray<ConvexPolyhedronCL>& convexPolyhedra = narrowphaseAndSolver->getCustomDispatchData()->m_convexPolyhedra;
		const btAlignedObjectArray<btGpuFace>& faces = narrowphaseAndSolver->getCustomDispatchData()->m_convexFaces;
		const btAlignedObjectArray<int>& convexIndices = narrowphaseAndSolver->getCustomDispatchData()->m_convexIndices;
		const btAlignedObjectArray<btVector3>& convexVertices = narrowphaseAndSolver->getCustomDispatchData()->m_convexVertices;

		for ( int i = 0; i < bodyArrayCPU.size(); i++ )
		{
			const RigidBodyBase::Body& body = bodyArrayCPU[i];

			u32 collidableIndex = body.m_collidableIdx;

			btVector3 pos(body.m_pos.x, body.m_pos.y, body.m_pos.z);
			btQuaternion rot(body.m_quat.x, body.m_quat.y, body.m_quat.z, body.m_quat.w);
			
			btTransform tr(rot, pos);

			int shapeType = collidables[collidableIndex].m_shapeType;
			int shapeIndex = collidables[collidableIndex].m_shapeIndex;
			const ConvexPolyhedronCL& convexShape = convexPolyhedra[shapeIndex];

			int numFaces = convexShape.m_numFaces;

			glPointSize(4.0f);
			
			btTransform trRot(rot, btVector3(0, 0, 0));

			for ( int f = 0; f < numFaces; f++ ) 
			{
				const btGpuFace& face = faces[convexShape.m_faceOffset + f];
				btVector3 n(face.m_plane.x, face.m_plane.y, face.m_plane.z);
				n = trRot*n;

				btVector3 center(0, 0, 0);

				for ( int v = 0; v < face.m_numIndices; v++ )
				{
					btVector3 vert = convexVertices[convexShape.m_vertexOffset + convexIndices[face.m_indexOffset + v]];
					vert = tr * vert;

					center += vert;
				}

				center /= face.m_numIndices;

				glBegin(GL_POINTS);
				glVertex3f(center.x(), center.y(), center.z());
				glEnd();

				btVector3 end = n + center;

				glBegin(GL_LINES);
				glVertex3f(center.x(), center.y(), center.z());
				glVertex3f(end.x(), end.y(), end.z());
				glEnd();
			}
		}
	}

	UpdateUI();
}


#include "../rendering/rendertest/OpenGLInclude.h"

#include "../opencl/gpu_rigidbody_pipeline/CommandLineArgs.h"

void Usage()
{
	printf("\nprogram.exe [--cl_device=<int>] [--benchmark] [--disable_opencl] [--cl_platform=<int>]  [--x_dim=<int>] [--y_dim=<num>] [--z_dim=<int>] [--x_gap=<float>] [--y_gap=<float>] [--z_gap=<float>]\n"); 
};


void	DumpSimulationTime(FILE* f)
{
	CProfileIterator* profileIterator = CProfileManager::Get_Iterator();

	profileIterator->First();
	if (profileIterator->Is_Done())
		return;

	float accumulated_time=0,parent_time = profileIterator->Is_Root() ? CProfileManager::Get_Time_Since_Reset() : profileIterator->Get_Current_Parent_Total_Time();
	int i;
	int frames_since_reset = CProfileManager::Get_Frame_Count_Since_Reset();
	
	//fprintf(f,"%.3f,",	parent_time );
	float totalTime = 0.f;

	
	int numChildren = 0;
	
	for (i = 0; !profileIterator->Is_Done(); i++,profileIterator->Next())
	{
		numChildren++;
		float current_total_time = profileIterator->Get_Current_Total_Time();
		accumulated_time += current_total_time;
		float fraction = parent_time > SIMD_EPSILON ? (current_total_time / parent_time) * 100 : 0.f;
		if (!strcmp(profileIterator->Get_Current_Name(),"stepSimulation"))
		{
			fprintf(f,"%.3f,\n",current_total_time);
		}
		totalTime += current_total_time;
		//recurse into children
	}

	
	
	
	CProfileManager::Release_Iterator(profileIterator);


}
extern const char* g_deviceName;

int main(int argc, char* argv[])
{
	CommandLineArgs args(argc,argv);
	GpuDemo::ConstructionInfo ci;

	if (args.CheckCmdLineFlag("help"))
	{
		Usage();
		return 0;
	}

	bool benchmark=args.CheckCmdLineFlag("benchmark");
	bool dump_timings=args.CheckCmdLineFlag("dump_timings");
	ci.useOpenCL =!args.CheckCmdLineFlag("disable_opencl");
	
	args.GetCmdLineArgument("cl_device", ci.preferredOpenCLDeviceIndex);
	args.GetCmdLineArgument("cl_platform", ci.preferredOpenCLPlatformIndex);
	args.GetCmdLineArgument("x_dim", ci.arraySizeX);
	args.GetCmdLineArgument("y_dim", ci.arraySizeY);
	args.GetCmdLineArgument("z_dim", ci.arraySizeZ);
	args.GetCmdLineArgument("x_gap", ci.gapX);
	args.GetCmdLineArgument("y_gap", ci.gapY);
	args.GetCmdLineArgument("z_gap", ci.gapZ);
	printf("Demo settings:\n");
	printf("x_dim=%d, y_dim=%d, z_dim=%d\n",ci.arraySizeX,ci.arraySizeY,ci.arraySizeZ);
	printf("x_gap=%f, y_gap=%f, z_gap=%f\n",ci.gapX,ci.gapY,ci.gapZ);
	
	printf("Preferred cl_device index %d\n", ci.preferredOpenCLDeviceIndex);
	printf("Preferred cl_platform index%d\n", ci.preferredOpenCLPlatformIndex);
	printf("-----------------------------------------------------\n");
	
	#ifndef BT_NO_PROFILE
	CProfileManager::Reset();
#endif //BT_NO_PROFILE

	

	bool syncOnly = false;

#ifdef __APPLE__
	window = new MacOpenGLWindow();
#else
	window = new Win32OpenGLWindow();
#endif
	btgWindowConstructionInfo wci(g_OpenGLWidth,g_OpenGLHeight);
	
	window->createWindow(wci);
	window->setWindowTitle("MyTest");
	window->setKeyboardCallback(MyKeyboardCallback);
	window->setMouseButtonCallback(MyMouseButtonCallback);
	window->setMouseMoveCallback(MyMouseMoveCallback);
	window->setWheelCallback(btDefaultWheelCallback);
	window->setResizeCallback(MyResizeCallback);
	printf("-----------------------------------------------------\n");

	// glewInit() should be called after Win32OpenGLWindow is created.
	GLenum err = glewInit();

	// setup Gwen GUI
	setupGUI(g_OpenGLWidth, g_OpenGLHeight);

	if (pCanvas)
	{
		pCanvas->SetSize(g_OpenGLWidth,g_OpenGLHeight);
	}

	glClearColor(1,0,0,1);
	glClear(GL_COLOR_BUFFER_BIT);	
	window->startRendering();
	glFinish();
	
//	GLPrimitiveRenderer prim(g_OpenGLWidth,g_OpenGLHeight);
	float color[4] = {1,1,1,1};
//	prim.drawRect(0,0,200,200,color);
	window->endRendering();
	glFinish();

	static bool once=true;
#ifdef _WIN32
	glewInit();
#endif
	
	once=false;

	//OpenGL3CoreRenderer render;
	InitGL();
	
	glClearColor(0,1,0,1);
	glClear(GL_COLOR_BUFFER_BIT);
	
	window->endRendering();

	glFinish();
	
	

	// my scene setting
	ci.useOpenCL = true;
	ci.arraySizeX = 10;
	ci.arraySizeY = 10;
	ci.arraySizeZ = 10;
	ci.gapX = 3.0;
	ci.gapY = 3.0;
	ci.gapZ = 3.0;

	{
		demo = new GpuDemo;
		demo->myinit();
		
		g_shapeDrawer = new GL_ShapeDrawer();

		demo->initPhysics(ci);
		//render.init();
		printf("-----------------------------------------------------\n");
		
		FILE* f = 0;
		if (benchmark)
		{
			char fileName[1024];
			sprintf(fileName,"%s_%d_%d_%d.txt",g_deviceName,ci.arraySizeX,ci.arraySizeY,ci.arraySizeZ);
			printf("Open file %s\n", fileName);


			f=fopen(fileName,"w");
			if (f)
				fprintf(f,"%s (%dx%dx%d=%d),\n",  g_deviceName,ci.arraySizeX,ci.arraySizeY,ci.arraySizeZ,ci.arraySizeX*ci.arraySizeY*ci.arraySizeZ);
		}

		printf("-----------------------------------------------------\n");
		do
		{
			if ( g_bReset )
			{
				demo->exitPhysics();
				delete demo;

				demo = new GpuDemo;
				demo->myinit();

				demo->initPhysics(ci);

				g_bReset = false;
			}
			
			window->startRendering();
			/*glClearColor(0.6,0.6,0.6,1);
			glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);*/
			
			if ( !g_bSimulationPause )
			{
				if ( g_bSimulationOneStep )
				{
					g_bSimulationOneStep = false;
					g_bSimulationPause = true;
				}

				demo->clientMoveAndDisplay();
			}
				
			
			
			Render();

			syncOnly = true;
			window->endRendering();
			glFinish();


		if (dump_timings)
			CProfileManager::dumpAll();

		if (f)
		{
			static int count=0;
			
			if (count>2 && count<102)
			{
				DumpSimulationTime(f);
			}
			if (count>=102)
				window->setRequestExit();
			count++;
		}

		} while (!window->requestedExit());

		demo->exitPhysics();
		delete demo;
		if (f)
			fclose(f);
	}

	if ( g_shapeDrawer )
	{
		delete g_shapeDrawer;
		g_shapeDrawer = NULL;
	}
	
	window->closeWindow();
	delete window;
	window = 0;


	return 0;
}