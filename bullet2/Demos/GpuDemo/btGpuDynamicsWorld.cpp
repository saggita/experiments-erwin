#include "btGpuDynamicsWorld.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"

#include "../../../opencl/gpu_rigidbody_pipeline2/CLPhysicsDemo.h"
#include "../../../opencl/gpu_rigidbody_pipeline/btGpuNarrowPhaseAndSolver.h"
#include "BulletCollision/CollisionShapes/btPolyhedralConvexShape.h"
#include "LinearMath/btQuickprof.h"
#include "../../../opencl/gpu_rigidbody_pipeline2/btGpuSapBroadphase.h"
#include "../../../opencl/softbodyCL/btSoftbodyCL.h"

#ifdef _WIN32
	#include <wiNdOws.h>
#endif

struct btGpuInternalData
{

};

btGpuDynamicsWorld::btGpuDynamicsWorld(int preferredOpenCLPlatformIndex,int preferredOpenCLDeviceIndex)
:btSoftRigidDynamicsWorld(NULL, NULL, NULL, NULL),
m_gravity(0,-10,0),
m_once(true)
{
	m_gpuPhysics = new CLPhysicsDemo(512*1024, MAX_CONVEX_BODIES_CL);
	bool useInterop = false;
	///platform and device are swapped, todo: fix this and make it consistent
	m_gpuPhysics->init(preferredOpenCLDeviceIndex,preferredOpenCLPlatformIndex,useInterop);

	m_pSoftBodySolverCL = new btSoftBodySimulationSolverOpenCL(m_gpuPhysics);
}

btGpuDynamicsWorld::~btGpuDynamicsWorld()
{
	delete m_gpuPhysics;

	if ( m_pSoftBodySolverCL )
	{
		delete m_pSoftBodySolverCL;
		m_pSoftBodySolverCL = NULL;
	}
}

void btGpuDynamicsWorld::exitOpenCL()
{
}






int		btGpuDynamicsWorld::stepSimulation( btScalar timeStep,int maxSubSteps, btScalar fixedTimeStep)
{
#ifndef BT_NO_PROFILE
	CProfileManager::Reset();
#endif //BT_NO_PROFILE

	BT_PROFILE("stepSimulation");

	//convert all shapes now, and if any change, reset all (todo)
	
	if (m_once)
	{
		m_once = false;
		m_gpuPhysics->writeBodiesToGpu();

		m_pSoftBodySolverCL->Initialize();
	}

	m_gpuPhysics->stepSimulation();

	{
		{
			BT_PROFILE("readbackBodiesToCpu");
			//now copy info back to rigid bodies....
			m_gpuPhysics->readbackBodiesToCpu();
		}

		
		{
			BT_PROFILE("scatter transforms into rigidbody");
			for (int i=0;i<this->m_collisionObjects.size();i++)
			{
				btVector3 pos;
				btQuaternion orn;
				m_gpuPhysics->getObjectTransformFromCpu(&pos[0],&orn[0],i);
				btTransform newTrans;
				newTrans.setOrigin(pos);
				newTrans.setRotation(orn);
				this->m_collisionObjects[i]->setWorldTransform(newTrans);
			}
		}
	}

	// simulate softbodies
	m_pSoftBodySolverCL->Integrate(timeStep);
	m_pSoftBodySolverCL->AdvancePosition(timeStep);
	m_pSoftBodySolverCL->UpdateBoundingVolumes(timeStep);
	m_pSoftBodySolverCL->ResolveCollision(timeStep);
	//m_pSoftBodySolverCL->ResolveCollisionCPU(timeStep);
	m_pSoftBodySolverCL->ReadBackFromGPU();
	

#ifndef BT_NO_PROFILE
	CProfileManager::Increment_Frame_Counter();
#endif //BT_NO_PROFILE


	return 1;
}


void	btGpuDynamicsWorld::setGravity(const btVector3& gravity)
{
}

void	btGpuDynamicsWorld::addRigidBody(btRigidBody* body)
{

	body->setMotionState(0);

	int index = m_uniqueShapes.findLinearSearch(body->getCollisionShape());
	if (index==m_uniqueShapes.size())
	{
		m_uniqueShapes.push_back(body->getCollisionShape());
		btAssert(body->getCollisionShape()->isPolyhedral());
		btPolyhedralConvexShape* convex = (btPolyhedralConvexShape*)body->getCollisionShape();
		int numVertices=convex->getNumVertices();
		
		int strideInBytes=sizeof(btVector3);
		btAlignedObjectArray<btVector3> tmpVertices;
		tmpVertices.resize(numVertices);
		for (int i=0;i<numVertices;i++)
			convex->getVertex(i,tmpVertices[i]);
		const float scaling[4]={1,1,1,1};
		bool noHeightField=true;
		
		int gpuShapeIndex = m_gpuPhysics->registerCollisionShape(&tmpVertices[0].getX(), strideInBytes, numVertices, scaling, noHeightField);
		m_uniqueShapeMapping.push_back(gpuShapeIndex);

	}

	int gpuShapeIndex= m_uniqueShapeMapping[index];
	float mass = body->getInvMass() ? 1.f/body->getInvMass() : 0.f;
	btVector3 pos = body->getWorldTransform().getOrigin();
	btQuaternion orn = body->getWorldTransform().getRotation();
	
	m_gpuPhysics->registerPhysicsInstance(mass,&pos.getX(),&orn.getX(),gpuShapeIndex,m_collisionObjects.size());

	m_collisionObjects.push_back(body);
}

void    btGpuDynamicsWorld::addSoftBody(btSoftbodyCL* softBody)
{
	const CAabb& aabb = softBody->GetAabb();

	btVector3 aabbMin(aabb.Min()[0], aabb.Min()[1], aabb.Min()[2]);
	btVector3 aabbMax(aabb.Max()[0], aabb.Max()[1], aabb.Max()[2]);

	m_gpuPhysics->registerSoftbodyInstance(aabbMin, aabbMax, m_collisionObjects.size());

	m_pSoftBodySolverCL->addSoftBody(softBody);
}

void	btGpuDynamicsWorld::removeCollisionObject(btCollisionObject* colObj)
{
	//btSoftRigidDynamicsWorld::removeCollisionObject(colObj);
	btDynamicsWorld::removeCollisionObject(colObj);
}

InternalData* btGpuDynamicsWorld::getInternalData() 
{ 
	btAssert(m_gpuPhysics);
	return m_gpuPhysics->m_data;
}

void btGpuDynamicsWorld::getAabbs(btAlignedObjectArray<btVector3>& mins, btAlignedObjectArray<btVector3>& maxs)
{
	mins.clear();
	maxs.clear();

	btGpuSapBroadphase* pSap = m_gpuPhysics->getGpuSapBroadphase();

	for ( int i = 0; i < pSap->m_aabbsCPU.size(); i++ )
	{
		btVector3 min;
		btVector3 max;

		for ( int j = 0; j < 3; j++ )
		{
			min[j] = pSap->m_aabbsCPU[i].m_min[j];
			max[j] = pSap->m_aabbsCPU[i].m_max[j];
		}

		mins.push_back(min);
		maxs.push_back(max);
	}

	int numOverlapping = pSap->m_overlappingPairsCPU.size();

	if ( numOverlapping > 0 )
		int sdfsdfsdf = 0;
}