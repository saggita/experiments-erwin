
#ifndef BT_CPU_DYNAMICS_WORLD_H
#define BT_CPU_DYNAMICS_WORLD_H

class btDefaultCollisionConfiguration;
class btCollisionDispatcher;
struct btDbvtBroadphase;
class btSequentialImpulseConstraintSolver;

#include "BulletSoftBody/btSoftRigidDynamicsWorld.h"

class btCpuDynamicsWorld : public btSoftRigidDynamicsWorld
{
	
public:
	
	btCpuDynamicsWorld();
	
	virtual ~btCpuDynamicsWorld();


};

#endif //BT_CPU_DYNAMICS_WORLD_H
