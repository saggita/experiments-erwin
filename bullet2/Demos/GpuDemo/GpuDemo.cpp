/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/


#include "btCpuDynamicsWorld.h"
#include "btGpuDynamicsWorld.h"
#include "BulletCollision\CollisionShapes\btTriangleIndexVertexArray.h"

#define SCALING 1.
#define START_POS_X -5
#define START_POS_Y -5
#define START_POS_Z -3

#include "LinearMath/btVector3.h"

#include "GpuDemo.h"
//#include "GlutStuff.h"
///btBulletDynamicsCommon.h is the main Bullet include file, contains most common include files.
//#include "btBulletDynamicsCommon.h"

#include "BulletCollision/CollisionShapes/btSphereShape.h"
#include "BulletCollision/CollisionShapes/btConvexHullShape.h"
#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"
#include "BulletSoftBody/btSoftBodySolvers.h"
#include "BulletSoftBody/btSoftBody.h"
#include "LinearMath/btDefaultMotionState.h"
#include "LinearMath/btQuickprof.h"


#include <stdio.h> //printf debugging


void GpuDemo::clientMoveAndDisplay()
{
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 

	//simple dynamics world doesn't handle fixed-time-stepping
	float dt = getDeltaTimeInSeconds();
	
	///step the simulation
	if (m_dynamicsWorld)
	{
		m_dynamicsWorld->stepSimulation(dt);		
		static int count=0;
		count++;
		if (count==5)
		{
			CProfileManager::dumpAll();
		}

		/*for ( int i = 0; i < (int)m_ClothArray.size(); i++ )
		{
			btSoftbodyCL* pCloth = m_ClothArray[i];

			pCloth->Integrate(dt);
			pCloth->AdvancePosition(dt);	
			pCloth->UpdateBoundingVolumes(dt);
		}*/
		
		

	}
		
	renderme(); 
	swapBuffers();
}



void GpuDemo::displayCallback(void) {

	
	renderme();

	//optional but useful: debug drawing to detect problems
	if (m_dynamicsWorld)
		m_dynamicsWorld->debugDrawWorld();

	swapBuffers();
}


btAlignedObjectArray<btVector3> vertices;

void	GpuDemo::initPhysics(const ConstructionInfo& ci)
{

	setTexturing(true);
	setShadows(false);

	setCameraDistance(btScalar(SCALING*50.));

	///collision configuration contains default setup for memory, collision setup
	if (ci.useOpenCL)
	{
		m_dynamicsWorld = new btGpuDynamicsWorld(ci.preferredOpenCLPlatformIndex,ci.preferredOpenCLDeviceIndex);
	} else
	{
		m_dynamicsWorld = new btCpuDynamicsWorld();
	}

	
	m_dynamicsWorld->setGravity(btVector3(0,-10,0));
	m_dynamicsWorld->setDebugDrawer(&m_DebugDrawer);

	///create a few basic rigid bodies

	btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(50.),btScalar(1.),btScalar(50.)));
//	btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0,1,0),50);
	
	
	//m_collisionShapes.push_back(groundShape);

	btTransform groundTransform;
	groundTransform.setIdentity();
	//groundTransform.setOrigin(btVector3(0,-50,0));
	groundTransform.setOrigin(btVector3(0, 0,0));

	//We can also use DemoApplication::localCreateRigidBody, but for clarity it is provided here:
	if (1)
	{
		// static ground box with zero mass
		btScalar mass(0.);

		//rigidbody is dynamic if and only if mass is non zero, otherwise static
		bool isDynamic = (mass != 0.f);

		btVector3 localInertia(0,0,0);
		if (isDynamic)
			groundShape->calculateLocalInertia(mass,localInertia);

		//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
		btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,groundShape,localInertia);
		btRigidBody* body = new btRigidBody(rbInfo);

		//add the body to the dynamics world
		m_dynamicsWorld->addRigidBody(body);
	}

	if (0)
	{
		//create a few dynamic rigidbodies
		// Re-using the same collision is better for memory usage and performance

		//vertices.push_back(btVector3(0,1,0));
		vertices.push_back(btVector3(1,1,1));
		vertices.push_back(btVector3(1,1,-1));
		vertices.push_back(btVector3(-1,1,-1));
		vertices.push_back(btVector3(-1,1,1));
		vertices.push_back(btVector3(1,-1,1));
		vertices.push_back(btVector3(1,-1,-1));
		vertices.push_back(btVector3(-1,-1,-1));
		vertices.push_back(btVector3(-1,-1,1));
			
		btPolyhedralConvexShape* colShape = new btConvexHullShape(&vertices[0].getX(),vertices.size());
		colShape->initializePolyhedralFeatures();


		btPolyhedralConvexShape* boxShape = new btBoxShape(btVector3(SCALING*1,SCALING*1,SCALING*1));
		boxShape->initializePolyhedralFeatures();

		//btCollisionShape* colShape = new btSphereShape(btScalar(1.));
		//m_collisionShapes.push_back(colShape);
		//m_collisionShapes.push_back(boxShape);

		/// Create Dynamic Objects
		btTransform startTransform;
		startTransform.setIdentity();

	

		float start_x = START_POS_X - (float)ci.arraySizeX/2.0f;
		float start_y = START_POS_Y;
		float start_z = START_POS_Z - (float)ci.arraySizeZ/2.0f;

		for (int k=0;k<ci.arraySizeY;k++)
		{
			int sizeX = k==0? 100 : ci.arraySizeX;
			int startX = k==0? -50 : 0;
			float gapX = k==0? 2.05f : ci.gapX;
			for (int i=0;i<sizeX;i++)
			{
				int sizeZ = k==0? 100 : ci.arraySizeZ;
				int startZ = k==0? -50 : 0;
				float gapZ = k==0? 2.05f : ci.gapZ;
				for(int j = 0;j<sizeZ;j++)
				{
					btCollisionShape* shape = k==0? boxShape : colShape;
						btScalar	mass = k==0? 0.f : 1.f;

					//rigidbody is dynamic if and only if mass is non zero, otherwise static
					bool isDynamic = (mass != 0.f);

					btVector3 localInertia(0,0,0);
					if (isDynamic)
						shape->calculateLocalInertia(mass,localInertia);

					startTransform.setOrigin(SCALING*btVector3(
										btScalar(startX+gapX*i + start_x),
										btScalar(20+ci.gapY*k + start_y),
										btScalar(startZ+gapZ*j + start_z)));

			
					//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
					btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
					btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,shape,localInertia);
					btRigidBody* body = new btRigidBody(rbInfo);
					

					m_dynamicsWorld->addRigidBody(body);
				}
			}
		}
	}

	// Adding a box rigidbody
	{
		btPolyhedralConvexShape* boxShape = new btBoxShape(btVector3(2.0f, 2.0, 4.0));
		boxShape->initializePolyhedralFeatures();
		//m_collisionShapes.push_back(boxShape);
		
		btTransform boxTransform;
		boxTransform.setIdentity();
		boxTransform.setRotation(btQuaternion(btVector3(1.0, 1.0, 1.0).normalize(), 57.0));

		btScalar mass(1.0f);
		btVector3 localInertia(0, 0, 0);

		if ( mass != 0 )
			boxShape->calculateLocalInertia(mass, localInertia);

		boxTransform.setOrigin(btVector3(0, 5.0, 0));

		btDefaultMotionState* myMotionState = new btDefaultMotionState(boxTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, boxShape, localInertia);
		btRigidBody* body = new btRigidBody(rbInfo);

		m_dynamicsWorld->addRigidBody(body);
	}

	// Adding another box rigidbody
	{
		btPolyhedralConvexShape* boxShape = new btBoxShape(btVector3(3.0f, 1.0, 5.0));
		boxShape->initializePolyhedralFeatures();
		//m_collisionShapes.push_back(boxShape);
		
		btTransform boxTransform;
		boxTransform.setIdentity();
		boxTransform.setRotation(btQuaternion(btVector3(1.0, 0.0, 1.0).normalize(), 45.0));

		btScalar mass(2.0f);
		btVector3 localInertia(0, 0, 0);

		if ( mass != 0 )
			boxShape->calculateLocalInertia(mass, localInertia);

		boxTransform.setOrigin(btVector3(8.0f, 7.0f, 0));

		btDefaultMotionState* myMotionState = new btDefaultMotionState(boxTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, boxShape, localInertia);
		btRigidBody* body = new btRigidBody(rbInfo);

		m_dynamicsWorld->addRigidBody(body);
	}

	// softbody
	//createFlag(30, 30, m_softbodies);
		

	if ( 1 )
	{
		btSoftbodyCL* pCloth = new btSoftbodyCL();
		//assert(pCloth->Load("..\\..\\bin\\circle2723.obj"));
		assert(pCloth->Load("..\\..\\bin\\circle14200.obj"));
		pCloth->SetVertexMass(1.0f);
		pCloth->TranslateW(0.0f, 15.0f, 0.0f);
		pCloth->SetGravity(btVector3(0, -9.8, 0));
		pCloth->SetKb(0.45f); 
		pCloth->SetKst(0.995f); 
		pCloth->SetFrictionCoef(0.5f);
		pCloth->SetNumIterForConstraintSolver(7);
		pCloth->SetMargin(0.1f);
		pCloth->GetVertexArray()[0].m_InvMass = 0;
		pCloth->GetVertexArray()[0].m_Vel = btVector3(3.0f, 0, 0);
		pCloth->Initialize();

		m_ClothArray.push_back(pCloth);

		((btGpuDynamicsWorld*)m_dynamicsWorld)->addSoftBody(pCloth);		
	}

	if ( 1 )
	{
		btSoftbodyCL* pCloth = new btSoftbodyCL();
		//assert(pCloth->Load("..\\..\\bin\\circle14200.obj"));
		assert(pCloth->Load("..\\..\\bin\\circle2723.obj"));
		pCloth->SetVertexMass(1.0f);
		pCloth->TranslateW(10.0f, 30.0f, 10.0f);
		pCloth->SetGravity(btVector3(0, -9.8, 0));
		pCloth->SetKb(0.45f); 
		pCloth->SetKst(0.995f); 
		pCloth->SetFrictionCoef(0.5f);
		pCloth->SetNumIterForConstraintSolver(7);
		pCloth->SetMargin(0.5f);
		pCloth->Initialize();	
		m_ClothArray.push_back(pCloth);

		((btGpuDynamicsWorld*)m_dynamicsWorld)->addSoftBody(pCloth);	
	}

	if ( 0 )
	{
		btSoftbodyCL* pCloth = new btSoftbodyCL();
		assert(pCloth->Load("..\\..\\bin\\circle14200.obj"));
		//assert(pCloth->Load("..\\..\\bin\\circle2723.obj"));
		pCloth->SetVertexMass(1.0f);
		pCloth->TranslateW(-10.0f, 30.0f, 10.0f);
		pCloth->SetGravity(btVector3(0, -9.8, 0));
		pCloth->SetKb(0.45f); 
		pCloth->SetKst(0.995f); 
		pCloth->SetFrictionCoef(0.5f);
		pCloth->SetNumIterForConstraintSolver(5);
		/*pCloth->AddPin(20);
		pCloth->AddPin(500);*/
		pCloth->SetMargin(0.5f);
		pCloth->Initialize();	
		m_ClothArray.push_back(pCloth);

		((btGpuDynamicsWorld*)m_dynamicsWorld)->addSoftBody(pCloth);	
	}
}

/*void	GpuDemo::clientResetScene()
{
	exitPhysics();
	initPhysics();
}
	*/


void	GpuDemo::exitPhysics()
{

	//cleanup in the reverse order of creation/initialization
	for ( int i = 0; i < (int)m_ClothArray.size(); i++ )
	{
		btSoftbodyCL* pCloth = m_ClothArray[i];

		if ( pCloth )
			delete pCloth;
	}

	m_ClothArray.clear();

	

	//remove the rigidbodies from the dynamics world and delete them
	int i;
	if (m_dynamicsWorld)
	{
		for (i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--)
		{
			btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
			btRigidBody* body = btRigidBody::upcast(obj);
			if (body && body->getMotionState())
			{
				delete body->getMotionState();
			}
			m_dynamicsWorld->removeCollisionObject( obj );
			delete obj;
		}
	}

	////delete collision shapes
	//for (int j=0;j<m_collisionShapes.size();j++)
	//{
	//	btCollisionShape* shape = m_collisionShapes[j];
	//	delete shape;
	//}
	//m_collisionShapes.clear();

	delete m_dynamicsWorld;
	m_dynamicsWorld=0;
	

	
}


void GpuDemo::createFlag(int width, int height, btAlignedObjectArray<btSoftBody *> &softbodies )
{
	// Allocate a simple mesh consisting of a vertex array and a triangle index array
	btIndexedMesh mesh;
	mesh.m_numVertices = width*height;
	mesh.m_numTriangles = 2*(width-1)*(height-1);

	btVector3 *vertexArray = new btVector3[mesh.m_numVertices];

	mesh.m_vertexBase = reinterpret_cast<const unsigned char*>(vertexArray);
	int *triangleVertexIndexArray = new int[3*mesh.m_numTriangles];	
	mesh.m_triangleIndexBase = reinterpret_cast<const unsigned char*>(triangleVertexIndexArray);
	mesh.m_triangleIndexStride = sizeof(int)*3;
	mesh.m_vertexStride = sizeof(btVector3);

	// Generate normalised object space vertex coordinates for a rectangular flag
	float zCoordinate = 0.0f;
	
	btMatrix3x3 defaultScale(5.f, 0.f, 0.f, 0.f, 20.f, 0.f, 0.f, 0.f, 1.f);
	for( int y = 0; y < height; ++y )
	{
		float yCoordinate = y*2.0f/float(height) - 1.0f;
		for( int x = 0; x < width; ++x )
		{			
			float xCoordinate = x*2.0f/float(width) - 1.0f;

			btVector3 vertex(xCoordinate, yCoordinate, zCoordinate);
			btVector3 transformedVertex = defaultScale*vertex;

			vertexArray[y*width + x] = btVector3(transformedVertex.getX(), transformedVertex.getY(), transformedVertex.getZ() );

		}
	}

	// Generate vertex indices for triangles
	for( int y = 0; y < (height-1); ++y )
	{
		for( int x = 0; x < (width-1); ++x )
		{	
			// Triangle 0
			// Top left of square on mesh
			{
				int vertex0 = y*width + x;
				int vertex1 = vertex0 + 1;
				int vertex2 = vertex0 + width;
				int triangleIndex = 2*y*(width-1) + 2*x;
				triangleVertexIndexArray[(mesh.m_triangleIndexStride*triangleIndex)/sizeof(int)] = vertex0;
				triangleVertexIndexArray[(mesh.m_triangleIndexStride*triangleIndex+1)/sizeof(int)+1] = vertex1;
				triangleVertexIndexArray[(mesh.m_triangleIndexStride*triangleIndex+2)/sizeof(int)+2] = vertex2;
			}

			// Triangle 1
			// Bottom right of square on mesh
			{
				int vertex0 = y*width + x + 1;
				int vertex1 = vertex0 + width;
				int vertex2 = vertex1 - 1;
				int triangleIndex = 2*y*(width-1) + 2*x + 1;
				triangleVertexIndexArray[(mesh.m_triangleIndexStride*triangleIndex)/sizeof(int)] = vertex0;
				triangleVertexIndexArray[(mesh.m_triangleIndexStride*triangleIndex)/sizeof(int)+1] = vertex1;
				triangleVertexIndexArray[(mesh.m_triangleIndexStride*triangleIndex)/sizeof(int)+2] = vertex2;
			}
		}
	}

	
	float rotateAngleRoundZ = 0.0;
	//float rotateAngleRoundX = 1.0;
	float rotateAngleRoundX = 3.14159f/2.0f;
	btMatrix3x3 defaultRotate;
	defaultRotate[0] = btVector3(cos(rotateAngleRoundZ), sin(rotateAngleRoundZ), 0.f); 
	defaultRotate[1] = btVector3(-sin(rotateAngleRoundZ), cos(rotateAngleRoundZ), 0.f);
	defaultRotate[2] = btVector3(0.f, 0.f, 1.f);
	btMatrix3x3 defaultRotateX;
	defaultRotateX[0] = btVector3(1.f, 0.f, 0.f);
	defaultRotateX[1] = btVector3( 0.f, cos(rotateAngleRoundX), sin(rotateAngleRoundX));
	defaultRotateX[2] = btVector3(0.f, -sin(rotateAngleRoundX), cos(rotateAngleRoundX));

	btMatrix3x3 defaultRotateAndScale( (defaultRotateX*defaultRotate) );

	btVector3 defaultTranslate(0.f, 20.f, 0);

	btTransform transform( defaultRotateAndScale, defaultTranslate );
	transform.setOrigin(defaultTranslate);

	btSoftBody *softBody = createFromIndexedMesh( vertexArray, mesh.m_numVertices, triangleVertexIndexArray, mesh.m_numTriangles, true );

	for( int i = 0; i < mesh.m_numVertices; ++i )
	{
		softBody->setMass(i, 10.f/mesh.m_numVertices);
	}
	softBody->setMass((height-1)*(width), 0.f);
	softBody->setMass((height-1)*(width) + width - 1, 0.f);
	softBody->setMass((height-1)*width + width/2, 0.f);
	softBody->m_cfg.collisions = btSoftBody::fCollision::CL_SS+btSoftBody::fCollision::CL_RS;	

	softBody->transform( transform );
		
	/*softBody->m_cfg.kLF = 0.15f;
	softBody->m_cfg.kDG	= 0.01f;*/
	softBody->m_cfg.kLF = 0.0f;
	softBody->m_cfg.kDG	= 0.0f;
	softBody->m_cfg.piterations = 10;
	softBody->m_cfg.aeromodel	=	btSoftBody::eAeroModel::V_TwoSidedLiftDrag;
	//softBody->setWindVelocity(btVector3(5.0, 0.0, 25.0));
	softBody->setWindVelocity(btVector3(0.0, 0.0, 0.0));

	softbodies.push_back( softBody );
	//m_dynamicsWorld->addSoftBody( softBody );	

	delete [] vertexArray;
	delete [] triangleVertexIndexArray;
}

btSoftBody* GpuDemo::createFromIndexedMesh( btVector3 *vertexArray, int numVertices, int *triangleVertexIndexArray, int numTriangles, bool createBendLinks )
{
	btAssert(m_dynamicsWorld != NULL);

	btSoftBody* softBody = new btSoftBody(&(m_dynamicsWorld->getWorldInfo()), numVertices, vertexArray, 0);
	btSoftBody::Material * structuralMaterial = softBody->appendMaterial();
	btSoftBody::Material * bendMaterial;
	if( createBendLinks )
	{
		bendMaterial = softBody->appendMaterial();
		bendMaterial->m_kLST = 0.7f;
	} else {
		bendMaterial = NULL;
	}
	structuralMaterial->m_kLST = 1.0;
	

	// List of values for each link saying which triangle is associated with that link
	// -1 to start. Once a value is entered we know the "other" triangle
	// and can add a link across the link
	btAlignedObjectArray<int> triangleForLinks;
	triangleForLinks.resize( numVertices * numVertices, -1 );
	int numLinks = 0;
	for( int triangle = 0; triangle < numTriangles; ++triangle )
	{
		int index[3] = {triangleVertexIndexArray[triangle * 3], triangleVertexIndexArray[triangle * 3 + 1], triangleVertexIndexArray[triangle * 3 + 2]};
		softBody->appendFace( index[0], index[1], index[2] );
		
		// Generate the structural links directly from the triangles
		testAndAddLink( triangleForLinks, softBody, triangle, triangleVertexIndexArray, numVertices, index[0], index[1], index[2], structuralMaterial, createBendLinks, bendMaterial );
		testAndAddLink( triangleForLinks, softBody, triangle, triangleVertexIndexArray, numVertices, index[1], index[2], index[0], structuralMaterial, createBendLinks, bendMaterial );
		testAndAddLink( triangleForLinks, softBody, triangle, triangleVertexIndexArray, numVertices, index[2], index[0], index[1], structuralMaterial, createBendLinks, bendMaterial);
	}

	return softBody;
}

// Helper to test and add links correctly.
// Records links that have already been generated
bool GpuDemo::testAndAddLink( btAlignedObjectArray<int> &trianglesForLinks, btSoftBody *softBody, int triangle, int *triangleVertexIndexArray, int numVertices, int vertex0, int vertex1, int nonLinkVertex, btSoftBody::Material *structuralMaterial, bool createBendLinks, btSoftBody::Material *bendMaterial )
{		
	if( trianglesForLinks[ numVertices * vertex0 + vertex1 ] >= 0 && createBendLinks)
	{
		// Already have link so find other triangle and generate cross link

		int otherTriangle = trianglesForLinks[numVertices * vertex0 + vertex1];
		int otherIndices[3] = {triangleVertexIndexArray[otherTriangle * 3], triangleVertexIndexArray[otherTriangle * 3 + 1], triangleVertexIndexArray[otherTriangle * 3 + 2]};

		int nodeA;
		// Test all links of the other triangle against this link. The one that's not part of it is what we want.
		if( otherIndices[0] != vertex0 && otherIndices[0] != vertex1 )
			nodeA = otherIndices[0];
		if( otherIndices[1] != vertex0 && otherIndices[1] != vertex1 )
			nodeA = otherIndices[1];
		if( otherIndices[2] != vertex0 && otherIndices[2] != vertex1 )
			nodeA = otherIndices[2];

		softBody->appendLink( nodeA, nonLinkVertex, bendMaterial );
	} else {
		// Don't yet have link so create it
		softBody->appendLink( vertex0, vertex1, structuralMaterial );

		// If we added a new link, set the triangle array
		trianglesForLinks[numVertices * vertex0 + vertex1] = triangle;
		trianglesForLinks[numVertices * vertex1 + vertex0] = triangle;

	}

	return true;
}